package com.example.activity1;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.View;
import android.content.Intent;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    static final int REQUEST_IMAGE_CAPTURE=1;
    static final int SELECT_IMAGE=1234;
    ImageView image_display;
    Button button;
    Button button2;
    TextView text;
    EditText edition;
    Uri imageUri;
    private void dispatchTakePictureIntent()
    {
        Intent takePictureIntent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(takePictureIntent.resolveActivity(getPackageManager())!=null)
        {
            startActivityForResult(takePictureIntent,REQUEST_IMAGE_CAPTURE);
        }
    }
    private void openGallery()
    {
        Intent openGalleryIntent=new Intent();
        openGalleryIntent.setType("image/*");
        openGalleryIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(openGalleryIntent.createChooser(openGalleryIntent,"select picture"),SELECT_IMAGE);
    }

    public void addTextToFile(String text) {
        File file = new File(MainActivity.this.getFilesDir(), "text");
        if (!file.exists()) {
            file.mkdir();
        }
        try {
            File gpxfile = new File(file, "sample");
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(edition.getText().toString());
            writer.flush();
            writer.close();
            Toast.makeText(MainActivity.this, "Saved your text", Toast.LENGTH_LONG).show();
            System.out.println(gpxfile.getPath());
        } catch (Exception e) { }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*TextView text=new TextView(this);
        text.setText("bonjour, ça va?");
        setContentView(text);*/
        /*Button button=new Button(this);
        button.setText("gallery");
        setContentView(button);*/
        button=findViewById(R.id.button_id);
        button2=findViewById(R.id.button2_id);
        text=findViewById(R.id.result_id);
        edition=findViewById(R.id.edittext_id);
        image_display=findViewById(R.id.image_id);
        /*final Button button=findViewById(R.id.button_id);
        final Button button2=findViewById(R.id.button2_id);
        final TextView text=findViewById(R.id.result_id);
        final EditText edition=findViewById(R.id.edittext_id);
        final ImageView image_display=findViewById(R.id.image_id);
        final ImageView image_display=findViewById(R.id.image_id);*/

        button.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                text.setText("gallery");

                openGallery();
            }
        });
        button2.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                text.setText("camera");
                dispatchTakePictureIntent();
            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == SELECT_IMAGE && resultCode == RESULT_OK){
            imageUri = data.getData();
            edition.setText(imageUri.getPath());
            addTextToFile(imageUri.getPath());
            image_display.setImageURI(imageUri);
        }
    }


}
